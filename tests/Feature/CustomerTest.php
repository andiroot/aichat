<?php

it('has customer is not eligible', function(){
    $response = $this->post('api/customers/check', [
        'customer_id' => 4,   
    ]);

    $response->assertStatus(422);
});

it('has customer is eligible', function(){
    $response = $this->post('api/customers/check', [
        'customer_id' => 1,   
    ]);

    $response->assertStatus(200);
});