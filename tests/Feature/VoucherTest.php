<?php

it('has submission success', function(){
    \App\Models\CustomerVoucher::where('customer_id', 1)
        ->update([
            'locked_at' => \Carbon\Carbon::now()
        ]);
    $response = $this->post('api/photos/validate', [
        'customer_id' => 1,  
        'path' => 'test/only.png' 
    ]);

    $response->assertStatus(200);
});

it('has error submission exceeds 10 minutes', function(){
    \App\Models\CustomerVoucher::where('customer_id', 1)
    ->update([
        'locked_at' => \Carbon\Carbon::now()->addMinutes(12)
    ]);
    $response = $this->post('api/photos/validate', [
        'customer_id' => 1,
        'path' => 'test/only.png' 
    ]);

    $response->assertStatus(422);
});