<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PurchaseTransaction>
 */
class PurchaseTransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'customer_id' => \App\Models\Customer::factory(),
            'total_spent' => fake()->randomElement([100, 200, 300]),
            'total_saving' => fake()->randomElement([50, 10, 20]),
            'transaction_at' => fake()->dateTimeBetween('-1 month', '+1 month')
        ];
    }
}
