<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PurchaseTransaction;

class PurchaseTransactionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'customer_id' => 1,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-08-10',   
            ],
            [
                'customer_id' => 1,
                'total_spent' => 30,
                'total_saving' => 30,
                'transaction_at' => '2022-08-12',   
            ],
            [
                'customer_id' => 1,
                'total_spent' => 20,
                'total_saving' => 20,
                'transaction_at' => '2022-08-14',   
            ],
            [
                'customer_id' => 1,
                'total_spent' => 20,
                'total_saving' => 20,
                'transaction_at' => '2022-08-15',   
            ],
            [
                'customer_id' => 2,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-08-02',   
            ],            
            [
                'customer_id' => 3,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-08-02',   
            ],            
            [
                'customer_id' => 4,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-06-02',   
            ],            
            [
                'customer_id' => 4,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-06-08',   
            ],
            [
                'customer_id' => 4,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-06-01',   
            ],            
            [
                'customer_id' => 4,
                'total_spent' => 50,
                'total_saving' => 50,
                'transaction_at' => '2022-05-08',   
            ]
        ];
        PurchaseTransaction::insert($data);
    }
}
