<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerVoucher extends Model
{
    use HasFactory;
    protected $table = 'customer_voucher';

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }
}
