<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\VoucherService;

class VoucherController extends Controller
{
    protected $service;

    public function __construct(VoucherService $service)
    {
        $this->service = $service;
    }

    public function submit(Request $request)
    {
        $submit = $this->service->submit($request);
        if(is_array($submit)){
            return $this->returnJson([
                'voucher_code' => $submit['voucher_code']
            ], 'Success validate image', 200, 'ok');
            
        }
        else{
            return $this->returnJson([], 'Failed to validate image or Submission timeout', 422, 'failed');
        }
    }
}
