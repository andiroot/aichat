<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CustomerService;

class CustomerController extends Controller
{
    protected $service;

    public function __construct(CustomerService $service)
    {
        $this->service = $service;
    }

    public function check(Request $request)
    {
        $check = $this->service->check($request);
        $json = [];
        $message = '';
        $status_code = 422;
        $status = 'failed';

        if($check === true){
            $message = 'Voucher successfully redeemed, voucher will available in 10 minutes.';
            $status_code = 200;
            $status = 'ok';
        }

        else{
            switch($check){
                case 'voucher_run_out' :
                    $message = 'Vouchers are run out, please try again later.';
                break;
    
                case 'customer_not_eligible' :
                    $message = 'You are not yet eligible to redeem this voucher.';
                break;
    
                case 'customer_already_redeemed':
                    $message = 'You are already redeemed voucher.';
                break;
            }
        }

        return $this->returnJson($json, $message, $status_code, $status);
    }
}
