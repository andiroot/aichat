<?php 

namespace App\Services;

use App\Repositories\CustomerRepository;
use App\Repositories\VoucherRepository;

class CustomerService
{    
    protected $repository;
    protected $voucherRepo;

    public function __construct(CustomerRepository $repository,
        VoucherRepository $voucherRepo)
    {
        $this->repository = $repository;
        $this->voucherRepo = $voucherRepo;
    }

    public function check($request)
    {
        $customer_id = $request['customer_id'];
        $isVoucherAvail = $this->voucherRepo->isAvailable();
        if(!$isVoucherAvail){
            return 'voucher_run_out';
        }

        $isEligible = $this->repository->isEligible($customer_id);
        if($isEligible === null){
            return 'customer_not_eligible';
        }

        $isAlreadyRedeem = $this->voucherRepo->isRedeemed($customer_id);
        if($isAlreadyRedeem){
            return 'customer_already_redeemed';
        }

        $this->voucherRepo->lockVoucher($customer_id);
        return true;
    }
}