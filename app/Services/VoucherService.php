<?php 

namespace App\Services;

use App\Services\API\ImageRecognition;
use App\Repositories\VoucherRepository;
use App\Repositories\CustomerVoucherRepository;
use Carbon\Carbon;

class VoucherService
{    
    protected $repository;

    public function __construct(VoucherRepository $repository, 
        CustomerVoucherRepository $customerVoucherRepository)
    {
        $this->repository = $repository;
        $this->customerVoucherRepository = $customerVoucherRepository;
    }

    public function submit($request)
    {
        $customer_id = $request['customer_id'];
        $imgRecog = new ImageRecognition();
        $customerVoucher = $this->customerVoucherRepository
        ->getByCustomerId($request['customer_id']);

        $diff = Carbon::now()
            ->diffInMinutes(Carbon::parse($customerVoucher->locked_at));
        if(!$imgRecog->validate($request['path']) || $diff > 10){
            $this->repository->removeLock($customer_id);
            return 'recog_failed_or_exceeds';
        }
        else{
            return [
                'voucher_code' => $customerVoucher->voucher->code
            ];
        }
    }
}