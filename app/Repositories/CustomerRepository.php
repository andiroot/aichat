<?php 

namespace App\Repositories;

use App\Models\Customer;
use Carbon\Carbon;
use DB;

class CustomerRepository
{
    protected $model;

    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    public function isEligible($customer_id)
    {
        return $this->model
            ->whereHas('purchase_transaction', function($query) use ($customer_id){
                $query->where('customer_id', $customer_id)
                    ->whereBetween('transaction_at', [Carbon::now()->subDays(30)->toDateString(), 
                    Carbon::now()->toDateString()]);
            })
            ->whereRaw('(SELECT COUNT(*) FROM purchase_transaction 
                WHERE customers.id = purchase_transaction.customer_id) > 3')
            ->whereRaw('(SELECT SUM(total_spent) FROM purchase_transaction 
                WHERE customer_id=1) > 100')
            ->first();
    }
}