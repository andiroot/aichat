<?php 

namespace App\Repositories;

use App\Models\CustomerVoucher;
use Carbon\Carbon;

class CustomerVoucherRepository
{
    protected $model;

    public function __construct(CustomerVoucher $model)
    {
        $this->model = $model;
    }

    public function getByCustomerId($customer_id)
    {
        return $this->model->where('customer_id', $customer_id)
            ->with('voucher')->first();
    }
}