<?php 

namespace App\Repositories;

use App\Models\Voucher;
use App\Models\CustomerVoucher;
use Carbon\Carbon;

class VoucherRepository
{
    protected $model;
    protected $customerVoucher;

    public function __construct(Voucher $model, CustomerVoucher $customerVoucher)
    {
        $this->model = $model;
        $this->customerVoucher = $customerVoucher;
    }

    public function isAvailable()
    {
        return $this->model->where('is_used', 0)->exists();
    }

    public function isRedeemed($customer_id)
    {
        return $this->model->whereHas('customer_voucher', function($query) use ($customer_id){
            $query->where('customer_id', $customer_id)
                ->where('is_redeemed', true);
        })->exists();
    }

    public function lockVoucher($customer_id)
    {
        $voucher = $this->model->where('is_used', 0)->first();
        $isExists = $this->customerVoucher
            ->where('customer_id', $customer_id)->exists();
        
        if(!$isExists){
            $insert = $this->customerVoucher->insert([
                'is_locked' => true,
                'locked_at' => Carbon::now(),
                'customer_id' => $customer_id,
                'voucher_id' => $voucher->id,
            ]);

            $this->model->where('id', $voucher->id)->update([
                'is_used' => 1
            ]);

            return $insert;
        }
    }

    public function removeLock($customer_id)
    {
        $custVoucher = $this->customerVoucher
            ->where('customer_id', $customer_id);

        $this->model->where('id', $custVoucher->first()->voucher_id)->update([
            'is_used' => 0
        ]);

        $custVoucher->update([
            'is_locked' => 0
        ]);
        
    }
}